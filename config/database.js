module.exports = {
    development: {
    url: 'postgres://macbook@localhost:5432/note_app',
    dialect: 'postgres'
  },
    production: {
    url: 'postgres://macbook@localhost:5432/note_app',
    dialect: 'postgres'
  },
    staging: {
    url: 'postgres://macbook@localhost:5432/note_app',
    dialect: 'postgres'
  },
    test: {
    url: process.env.DATABASE_URL || 'postgres://macbook@localhost:5432/note_app',
    dialect: 'postgres'
  }
};